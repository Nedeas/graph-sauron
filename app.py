# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd

from sqlalchemy.sql import select
from sqlalchemy import create_engine

app = Dash(__name__)


engine = create_engine('postgresql+psycopg2://postgres:pwd@127.0.0.1:5432/bdd')
with engine.connect() as conn:
    df = pd.read_sql("""
                     select site_id, dates, reponse_time
                     from \"checks\"
                     order by site_id, dates asc
                     """
                     , conn)
    fig = px.line(df, y="reponse_time", x="dates", color="site_id")

    app.layout = html.Div(children=[
        html.H1(children='Graph Test'),
        dcc.Graph(
            id='graph-test',
            figure=fig
        )
    ])

if __name__ == '__main__':
    app.run_server(debug=True)
