INSTALLATION EN VIRTUALENV PYTHON
---------------------------------

# Préparation environnement

Nous allons installer l'application via un `virtualenv` de python.
Voici les prérequis :
- Python 3.8+
- Virtualenv (`pip3 install virtualenv`)

1. virtualenv -p $(which python3) .venv
2. source .venv/bin/activate
3. pip3 install -r requirements.txt

# Lancement

```SHELL
python3 app.py
```

# Modifier la connexion

A la ligne 14 de `app.py` modifier les paramétres de connexion.

# Enjoy

Connectez vous à l'adresse indiquer par la commande `python3 app.py`.
Normalement : http://127.0.0.1:8050/

